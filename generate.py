import os
import re
import markdown
import jinja2

html_path = "public"

def main():
    for folder in ("en", "he"):
        for name in os.listdir(f"pages/{folder}"):
            output = name.replace(".md", ".html")
            render(
                source=f"pages/{folder}/{name}",
                template="index.html",
                filename=f"{folder}/{output}",
            )

    render(
        source="pages/index.md",
        template="index.html",
        filename="index.html",
    )


def render(source, template, filename=None, **args):
    with open(source) as fh:
        text = fh.read()

    match = re.search("^---\n(.*)\n---\n(.*)$", text, re.DOTALL)
    if not match:
        raise Exception(f"parsing error in {source}")
    (header, content) = match.groups()
    headers = {}
    for row in header.split("\n"):
        (key, value) = row.split(":")
        headers[key] = value

    content = markdown.markdown(content, extensions=['tables'])
    content = re.sub('<h1>', '<h1 class="title is-1">', content)
    content = re.sub('<h2>', '<h2 class="title is-2">', content)
    content = re.sub('<h3>', '<h3 class="title is-3">', content)

    root = os.path.dirname(os.path.abspath(__file__))
    templates_dir = os.path.join(root, "templates")
    env = jinja2.Environment(loader=jinja2.FileSystemLoader(templates_dir), autoescape=True)
    html_template = env.get_template(template)
    lang = "en"
    if filename.startswith("en/"):
        lang = "en"
    if filename.startswith("he/"):
        lang = "he"
    html = html_template.render(**args, **headers, content=content, lang=lang)

    full_path = os.path.join(html_path, filename)
    dir_path = os.path.dirname(full_path)
    os.makedirs(dir_path, exist_ok=True)

    with open(full_path, "w") as fh:
        fh.write(html)

main()
