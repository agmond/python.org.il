---
title: PyCon Israel
---

# PyCon IL

The annual Python conference in Israel [PyCon IL](https://pycon.org.il) has its own site.

If you would like to make sure you don't miss it next year, subscribe to the [PyCon IL newsletter](https://lists.hamakor.org.il/postorius/lists/news.pycon.org.il/).


